# testing forgetting the Pilatus PC12 Cond Lever position and to address 
# the Condition lever problem when it is set to close and the engine keeps on running

from fly import * 
	
def Main(dt):
	if IsSimulating():
		user_aircraft = GetObjectName()

		if user_aircraft == "SpongeBob PC-12": #Using spongebobo PC-12 mod

			cond = Message(MSG_GETDATA, ID("cond"), ID("st8t"))
			SendMessage(cond)
            NoticeToUser(str(cond['realData']), 1.0)
            $ test successful on getting the condition lever position !

            # text file for debug purposes, this way i got the "realData" key of the cond dictionary
            #text_file = open('C:\Users\Leonel\Desktop\pilatus.txt','a')
			#text_file.write(str(cond))
			#text_file.close()

			NoticeToUser(str(cond['realData']), 1.0)
